{ config, pkgs, lib, ... }: {
  config = {
    home.packages = with pkgs; [
      # for node/js
      nodePackages.typescript
      nodePackages.typescript-language-server
      nodejs
      yarn
    ];
    programs.emacs.extraPackages = lib.mkIf config.programs.emacs.enable (epkgs:
      with epkgs; [
        #javascript
        rjsx-mode
        typescript-mode
        js2-refactor
        npm-mode
        nodejs-repl
        skewer-mode
        xref-js2
      ]
    );
  };
}
