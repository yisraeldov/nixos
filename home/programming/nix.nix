{ config, pkgs, lib, ... }:
let unstable = import <nixos-unstable> { };
in {
  config = {
    home.packages = [
      unstable.nil # nix lanaguage server
      pkgs.nixfmt-classic
    ];
    programs.emacs.extraPackages = lib.mkIf config.programs.emacs.enable (epkgs:
      with epkgs; [
        nix-mode
      ]
    );
  };
}


