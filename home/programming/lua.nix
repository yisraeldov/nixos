{ config, pkgs, lib, ... }:
{
  config = {
    home.packages = with pkgs; [
      luajit
      lua-language-server
    ];
    programs.emacs.extraPackages = lib.mkIf config.programs.emacs.enable (epkgs:
      with epkgs; [
        lua-mode
      ]
    );
    programs.emacs.extraConfig = ''
    ;(ydl/load-language-package "lua")
    '';
  };
  
}
