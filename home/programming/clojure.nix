{ config, pkgs, lib, ... }: {
  config = {
    home.packages = with pkgs; [
      clojure
      clj-kondo
      clojure-lsp
      babashka
    ];
    programs.emacs.extraPackages = lib.mkIf config.programs.emacs.enable (epkgs:
      with epkgs; [
        clojure-mode
        cider
        clj-refactor
      ]
    );

  };
}
