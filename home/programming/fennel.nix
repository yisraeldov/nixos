{ config, pkgs, lib, ... }:
let local-pkgs = import /home/yisraeldov/Projects/nixpkgs { };
in {
  config = {
    home.packages = with pkgs; [
      fennel
      fnlfmt
#      local-pkgs.fennel-ls
    ];
    programs.emacs.extraPackages = lib.mkIf config.programs.emacs.enable (epkgs:
      with epkgs; [
        fennel-mode
      ]
    );
    programs.emacs.extraConfig = ''
    ;(ydl/load-language-package "fennel")
    '';
  };
}
