{ config, pkgs, lib, ... }:
let unstable = import <nixos-unstable> { };
in {
  config = {
    home.packages = with pkgs; [
      php
      phpPackages.composer
      unstable.phpactor
    ];
    programs.emacs.extraPackages = lib.mkIf config.programs.emacs.enable (epkgs:
      with epkgs; [
        php-mode
      ]
    );
  };
}
