{ config, pkgs, lib, ... }:
with lib;
let
  unstable = import <nixos-unstable> { };
  #pidgin-gpg = pkgs.callPackage ./pidgin-gpg.nix { };
  # nix-software-center = import (pkgs.fetchFromGitHub {
  #   owner = "vlinkz";
  #   repo = "nix-software-center";
  #   rev = "0.1.2";
  #   sha256 = "xiqF1mP8wFubdsAQ1BmfjzCgOD3YZf7EGWl9i69FTls=";
  # }) { };
in {
  options = {
    linuxDesktop.enable = mkEnableOption "Enable linux desktop apps";
  };

  imports = [
    ./gnome.nix
    ./kde.nix
  ];
  config = mkIf config.linuxDesktop.enable {

    #    xsession.enable = true;
    #    xsession.windowManager.command = ''
    #   "${pkgs.cinnamon.cinnamon-session}/bin/cinnamon-session"
    # '';

    programs.kde-pim.enable = false;
    home.packages = with pkgs; [
      barrier
      #nix-software-center
      electrum # bitcoin wallet
      #etcher
      gnome3.cheese

      kdeconnect
      linphone

      obs-studio

      libsForQt5.qtstyleplugins
      python3
      #qbittorrent #broke
      transmission-gtk
      #paprefs
      shutter
      #        unstable.deltachat-electron

      xournalpp
      # (python38Packages.py3status.overrideAttrs (oldAttrs: {
      #   propagatedBuildInputs = with python38Packages;
      #     [ pytz tzlocal ] ++ oldAttrs.propagatedBuildInputs;
      # }))
      libgtop
      simple-scan


      #mail
#      thunderbird
#      claws-mail
#      evolution

      #music players
      amberol
      #g4music
      pragha
      xsel
      slack
    ];
    qt = {
      enable = true;
      # platformTheme.name = "gtk";
      # style = {
      #   name = "adwaita-dark";
      #   package = pkgs.adwaita-qt;
      # };
    };
    gtk = {
      enable = true;
      gtk3.extraConfig = {
        Settings = ''
          gtk-application-prefer-dark-theme=1
        '';
      };
      gtk4.extraConfig = {
        Settings = ''
          gtk-application-prefer-dark-theme=1
        '';
      };

    };

    home.keyboard.options = [ "grp:caps_toggle" "grp_led:scroll" ];
    programs.pidgin = {
      enable = true;
      plugins = [
        pkgs.pidgin-otr
        pkgs.pidgin-osd
        pkgs.pidgin-window-merge
        pkgs.purple-plugin-pack
        pkgs.purple-hangouts
        #pkgs.purple-matrix
        pkgs.purple-lurch
        pkgs.pidgin-xmpp-receipts
        pkgs.pidgin-opensteamworks
        pkgs.pidgin-carbons
        pkgs.purple-xmpp-http-upload
        pkgs.purple-discord
        #pidgin-gpg
      ];
    };

    programs.bash.enableVteIntegration = true;

    # TODO: Find a way to enable darkmode brave://flags/#enable-force-dark
    programs.chromium = {
      enable = true;
      package = pkgs.brave;
    };

    xdg.enable = true;


#    services.password-store-sync.enable = true;

    services.gpg-agent = {
      verbose = true;
      enable = true;
      enableSshSupport = true;
      enableExtraSocket = true;
      sshKeys = [ "5F6ECFD9AE6B412D8587C64F91A7B8B663F63AD3" ];
      defaultCacheTtl = 24 * 30 * 60 * 60;
      maxCacheTtl = 24 * 30 * 60 * 60;
    };
    services.kdeconnect = {
      enable = true;
      indicator = true;
    };
  };
}
