{ config, pkgs, lib, ... }:
with lib; {
  accounts.email.accounts.jellop = {
    realName = "Yisrael Dov Lebow";
    address = "yisrael@jellop.com";
    gpg.signByDefault = true;
  };
  home.packages = [ pkgs.slack ];
}
