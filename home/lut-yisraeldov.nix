{ config, pkgs, lib, ... }:
with lib;
let
  emailAddress = "yisraeldov.lebow@idt.net";
  fullName = "Yisrael Dov Lebow";
in {
  targets.genericLinux.enable = true;

  services.gpg-agent = {
    verbose = true;
    enable = true;
    pinentryFlavor = "gnome3";
    enableSshSupport = true;
    enableExtraSocket = true;
    sshKeys = [ "B6C98D776DDACF883960640597788C00331C1F51" ];
    defaultCacheTtl = 24 * 30 * 60 * 60;
    maxCacheTtl = 24 * 30 * 60 * 60;
  };
  programs.git = lib.mkForce {
    delta.enable = true;
    signing.signByDefault = true;
    signing.key = emailAddress;
    userEmail = emailAddress;
    userName = fullName;
    ignores = [ "*~" "\\#*\\#" ".\\#*" ];
  };

  accounts.email.accounts = lib.mkForce {
    idt = {
      primary = lib.mkDefault true;
      realName = fullName;
      address = emailAddress;
      gpg.signByDefault = true;
    };
  };
  home.packages = [ pkgs.slack ];
}
