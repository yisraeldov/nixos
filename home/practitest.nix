{ config, pkgs, lib, ... }:
with lib;
let
  unstable = import <nixos-unstable> { };

  pkgsM1 = import <nixpkgs> { localSystem = "aarch64-darwin"; };

  emailAddress = "yisrael@practitest.com";
  fullName = "Yisrael 🐻 Lebow";
in {
  programs.git = lib.mkForce {
    delta.enable = true;
    signing.signByDefault = true;
    signing.key = emailAddress;
    userEmail = emailAddress;
    userName = fullName;
    ignores = [ "*~" "\\#*\\#" ".\\#*" ];
    package = pkgsM1.gitFull;
  };

  #this needs to be used for darwin
  home.file.gpgSshKeys = lib.mkIf (pkgs.stdenv.isDarwin) {
    target = ".gnupg/sshcontrol";
    text = (concatStringsSep "\n" [
      ("8B747AB47BFCB37CCAD193C28E41247E85E36F6E "
        + builtins.toString (60 * 60 * 24 * 7))
    ]) + "\n";
  };

  accounts.email.accounts = {
    lebowtech.primary = false;
    practitest = {
      primary = true;
      realName = fullName;
      address = emailAddress;
      gpg.signByDefault = true;
      imap.host = "imap.gmail.com";
      imapnotify.enable = false;
      imapnotify.boxes = [ "INBOX" ];
      imapnotify.onNotifyPost = "${pkgs.isync}/bin/mbsync -a ;"
        + "${pkgs.libnotify}/bin/notify-send 'New mail'";
      smtp.host = "smtp.gmail.com";
      userName = emailAddress;
      passwordCommand = "pass practitest/${emailAddress}";
      mbsync.enable = true;
      mbsync.create = "both";
      mbsync.expunge = "both";
    };
  };

  programs.emacs.enable = true;
  programs.emacs.package = pkgsM1.emacs;
  home.packages = with pkgsM1; [ gnupg slack ];
}
