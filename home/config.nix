{
  allowUnfree = true;
  nix.auto-optimise-store = true;
  #  nix.localSystem = "x86_64-darwin";
  nix.extra-platforms = [ "aarch64-darwin" "x86-darwin" ];
}

