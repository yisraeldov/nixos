{config, pkgs, lib, ...}:
with lib;
let
  cfg = config.programs.kde-pim;
in {
  options.programs.kde-pim = {
    enable = lib.mkEnableOption "KDE PIM base packages";
    kmail = lib.mkEnableOption "KMail" ;
    kontact = lib.mkEnableOption "Kontact" ;
    merkuro = lib.mkEnableOption "Merkuro" ;
  };

  
  config = lib.mkIf cfg.enable {
    programs.kde-pim.kmail = mkDefault true;
    programs.kde-pim.kontact = mkDefault true;
    programs.kde-pim.merkuro = mkDefault true;
    home.packages = with pkgs.kdePackages; [
      # core packages
      akonadi
      kdepim-runtime
    ] ++ lib.optionals cfg.kmail [
      akonadiconsole
      akonadi-search
      kmail
      kmail-account-wizard
    ] ++ lib.optionals cfg.kontact [
      kontact
    ] ++ lib.optionals cfg.merkuro [
      merkuro
    ];
  };
}
