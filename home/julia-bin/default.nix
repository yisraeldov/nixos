
{ stdenv, fetchurl }:
let
  version = "1.5.4";
in
stdenv.mkDerivation {
  pname = "juliabin";
  name = "juliabin";
  src = fetchurl {
    url = "https://julialang-s3.julialang.org/bin/linux/x64/1.5/julia-${version}-linux-x86_64.tar.gz";
    sha256 = "1icb3rpn2qs6c3rqfb5rzby1pj6h90d9fdi62nnyi4x5s58w7pl0";
  };  
}

