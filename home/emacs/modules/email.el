;; -*- lexical-binding: t; -*

;;sets up sending mail using msmtp
(setq
     message-send-mail-function #'message-send-mail-with-sendmail
      message-sendmail-envelope-from 'header
      message-sendmail-extra-arguments '("--read-envelope-from")
      message-sendmail-f-is-evil t

      send-mail-function #'smtpmail-send-it
      sendmail-program (executable-find "msmtp")

      mu4e-change-filenames-when-moving 't
      mu4e-get-mail-command "mbsync lebowtech:INBOX gmail:INBOX bhbsdventures:INBOX"
      mu4e-update-interval 180
      mu4e-bookmarks '((:name "Inboxes" :query "maildir:/Inbox/ AND  NOT flag:trashed" :key ?i  )
		       (:name "Unread messages" :query "flag:unread AND NOT flag:trashed" :key 117)
		       (:name "Today's messages" :query "date:today..now" :key 116)
		       (:name "Last 7 days" :query "date:7d..now" :hide-unread t :key 119)
		       (:name "Messages with images" :query "mime:image/*" :key 112))
      )

(defun ydl/mu4e-match-func
    (mail-address)
    "Matches by :to on email"
    (lambda (msg)
      (message mail-address)
      (when msg
	(mu4e-message-contact-field-matches msg
					    :to mail-address))))
(defun ydl-make-mu4e-contexts ()
    (setq mu4e-contexts
	  `(
	    ,(make-mu4e-context
	      :name "lebowtech"
	      :match-func (ydl/mu4e-match-func "lebow@lebowtech.com")
	     :vars '((user-mail-address    ."lebow@lebowtech.com")
		    (mu4e-sent-folder       . "/lebowtech/Sent")
		    (mu4e-drafts-folder     . "/lebowtech/Drafts")
		    (mu4e-trash-folder      . "/lebowtech/Trash")
		    (mu4e-refile-folder     . "/lebowtech/Archive")))
	    ,(make-mu4e-context
	      :name "gmail"
	      :match-func (ydl/mu4e-match-func "yisraeldov@gmail.com")
	      :vars `((user-mail-address  . "yisraeldov@gmail.com")
		      (mu4e-sent-folder   . "/sent")
					;gmail handles sent messages but I want it to keep local any way
		     (mu4e-drafts-folder  . "/gmail/[Gmail]/Drafts")
		     (mu4e-trash-folder   . "/gmail/[Gmail]/Trash")
		     (mu4e-refile-folder  . "/gmail/[Gmail]/All Mail")))
	    ,(make-mu4e-context
	      :name "bhbsdventures"
	      :match-func (ydl/mu4e-match-func "yisraldov@bhbsdventures.com")
	      :vars `((user-mail-address      . "yisraeldov@bhbsdventures.com")
		      (mu4e-sent-folder       . "/bhbsdventures/Sent")
		      (mu4e-drafts-folder     . "/bhbsdventures/Drafts")
		      (mu4e-trash-folder      . "/bhbsdventures/Trash")
		      (mu4e-refile-folder     . "/bhbsdventures/Archive"))))))


(use-package  mu4e
  :config (ydl-make-mu4e-contexts)
  (setq mail-user-agent 'mu4e-user-agent))

(use-package org-msg
  :straight 't
  :custom
  (org-msg-posting-style  nil)
  (org-msg-default-alternatives '(
				  (new . (utf-8 html))
				  (reply-to-html . (utf-8 html))
				  (reply-to-text  utf-8)))
  :config (org-msg-mode))
