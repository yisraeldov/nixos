;;; package --- Summary my ai
;;; Commentary: does ai type stuff
;;; Code:

(use-package llm
  :straight  t
  :custom (llm-warn-on-nonfree nil "Don't warn when using chatgpt"))

(use-package ellama
  :straight t
  :init
  ;; setup key bindings
  (setopt ellama-keymap-prefix "C-c e")
  ;; language you want ellama to translate to
  (setopt ellama-language "hebrew")
  (setopt ellama-auto-scroll 't)

  ;; could be llm-openai for example
  (require 'llm-openai)

  (let (deepseek-key (password-store-get "deepseek_api_key"))
    (if deepseek-key
	(setopt ellama-provider (make-llm-openai-compatible
				 :key deepseek-key
				 :url "https://api.deepseek.com/v1"
				 :chat-model "deepseek-chat"))
      (setopt ellama-provider (make-llm-openai
			       :key (password-store-get "openapi_api_key")
			       :chat-model "gpt-4o")))))



(setq ellama-rewrite-hebrew-prompt-template
      "
You are tasked with rewriting small portions of an English letter or message into Hebrew in the style and tone of classical rabbinic or Hassidic writings. The output should:

1. **Language and Style**:  
   - Use formal and traditional Hebrew, incorporating common abbreviations (e.g., ז\"ל, בעזה\"י, תובב\"א).  
   - Avoid unnecessary embellishments—stay focused on the original message's content and tone. 

2. **Content and Meaning**:  
   - Preserve the meaning, details, and tone of the original English text without omitting or altering the intention of message.

   - Do not add honorifics, blessings, or invocations unless they were present in the original message.  

3. **Structure**:  
   - Rewrite only the small portions provided, keeping them concise and faithful to the original.  
   - Maintain the same structure and emphasis as the source material.  

4. **Formatting**:  
   - Ensure the Hebrew is clear and traditional, adhering to the norms of rabbinic or Hassidic writing styles.  

Use the Yiddish spelling for any names that are not Hebrew names. For example Mendel = מענדל . But shabbos = שבת. Slonim = סלאנים (never with a vav)

Try to use idioms that are used in chasidic rabinical writtings. Any time there is a word that is transliterated from english to hebrew, just transliterate it back.

When I provide a segment of text, rewrite it accurately and modestly into Hebrew without adding extraneous elements.
")

(defun ellama-rewrite-hebrew (&optional edit-template)
  "Enhance the grammar and spelling in the currently selected region or buffer.
When the value of EDIT-TEMPLATE is 4, or with one `universal-argument' as
prefix (\\[universal-argument]), prompt the user to amend the template."
  (interactive "p")
  (ellama-change ellama-rewrite-hebrew-prompt-template edit-template))



