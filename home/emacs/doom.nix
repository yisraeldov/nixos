{ config, pkgs, lib, ... }:
with lib;
let
  cfg = config.programs.emacs.doom;
  doomDir = ".doom.emacs.d";
in {
  options.programs.emacs.doom = {
    enable = lib.mkEnableOption "Enable Doom Emacs" ;
  };
  config = lib.mkIf cfg.enable {
    # see https://discourse.nixos.org/t/how-do-i-clone-a-github-repo-to-a-folder-in-my-home-directory/29516
    home.sessionVariables = {
      DOOMDIR = "${config.home.homeDirectory}/.doom.d";
      #this is deprecated
      DOOMLOCALDIR = "${config.home.homeDirectory}/.local/doom";
    };
    home.file = {
      doomEmacs = {
        target = doomDir;
        source = builtins.fetchGit {
          url = "https://github.com/hlissner/doom-emacs";
          rev = "9c8cfaadde1ccc96a780d713d2a096f0440b9483";
          shallow = true;
        };
        recursive = true;
      };
    };
    home.sessionPath = [
      "${config.home.homeDirectory}/${doomDir}/bin"
    ];
    xdg.desktopEntries.doomEmacs = {
      type = "Application";
      name = "Doom Emacs";
      exec = "${pkgs.emacs}/bin/emacs --init-directory ${config.home.homeDirectory}/${doomDir}";
    };
  };
}
