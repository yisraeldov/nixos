{ config, pkgs, lib, ... }:
with lib; {
  options = {
    langs =  mkOption {
      description = "specific langague settings, mostly should be enabled";
      type =  types.listOf types.str;
    };
  };
  imports = [./doom.nix];
  config = mkIf config.programs.emacs.enable {
    programs.emacs.package = pkgs.emacs29-gtk3;
    programs.emacs.extraConfig = ''
    (message "hello extra config")
    (push "${ toString ./. }" load-path)
    (provide 'default)
    '';
    programs.emacs.doom.enable = true;
    programs.emacs.extraPackages = (epkgs:
      # nix-env -f '<nixpkgs>' -qaP -A emacsPackages
      with epkgs; [
        mu4e
        restart-emacs
        magit
        emacsql
        use-package
        company
        cape
        emacs-everywhere

        corfu
        corfu-terminal
        nerd-icons-corfu

        projectile

        #documentation
        devdocs
        editorconfig

        eglot
        vertico
        orderless
        envrc
        direnv

        marginalia

        yasnippet
        yasnippet-snippets

        smartparens
        exec-path-from-shell

        all-the-icons
        all-the-icons-dired
        nerd-icons
        nerd-icons-dired
        nerd-icons-completion

        doom-themes
        doom-modeline

        wanderlust

        treemacs
        treemacs-nerd-icons

        markdown-mode
        markdown-toc

        languagetool


        lua-mode

        yaml-mode
        tree-sitter
        tree-sitter-langs
        treesit-grammars.with-all-grammars
        rg
        which-key
        multiple-cursors

        activity-watch-mode
      ]
    );
    home.file = {
      ".emacs.d/init.el".source = ./init.el;
#      ".emacs.d/snippets".source = ./snippets; 
      ".emacs.d/langs".source = ./langs;
      ".emacs.d/modules".source = ./modules;
    };
    home.packages = with pkgs;
      [
        # for emacs-everywhere
        xclip
        xdotool
        yaml-language-server
        ripgrep
      ];
  };
}
