(use-package
  fennel-mode
  :mode ("\\.fnl\\'")
  :config (add-to-list 'eglot-server-programs '(fennel-mode . ("fennel-ls"))))

(provide 'fennel)
