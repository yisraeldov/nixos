(use-package
  php-mode
  :init (setq phpactor-executable (executable-find "phpactor"))
  :hook ((php-mode . eglot-ensure)
	 (php-mode . (lambda () (setq-local phpactor-executable (executable-find "phpactor")))))
  :mode ("\\.php\\'"))


(use-package phpactor
  :after php-mode
  :straight 't)

