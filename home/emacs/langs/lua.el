(use-package lua-mode
  :mode ("\\.lua\\'"))

(provide 'lua)
;;; lua.el ends here

