;;; Comentary: My emacs config
;; Set up
(set-frame-font "Victormono Nerd Font-14:weight=medium" nil 't)

;; sets a sane hebrew font אבגדגדגכגד
(set-fontset-font "fontset-default" '(#x0590 . #x05FF) "Miriam Mono CLM")



(tool-bar-mode -1)
(menu-bar-mode -1)
(scroll-bar-mode -1)

(load-theme 'doom-tomorrow-night t)
(doom-modeline-mode )
(require 'use-package)
(use-package magit)


(prefer-coding-system 'utf-8)


(setq whitespace-style '(face trailing tabs spaces space-before-tab space-after-tab))
(whitespace-mode 1)
(add-hook 'prog-mode-hook 'whitespace-mode)


;; set up straight.el
(defvar bootstrap-version)
(let ((bootstrap-file
       (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
      (bootstrap-version 6))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
        (url-retrieve-synchronously
         "https://raw.githubusercontent.com/radian-software/straight.el/develop/install.el"
         'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))


(use-package treemacs)

(use-package treemacs-nerd-icons)

(use-package emacs-everywhere
  :custom (emacs-everywhere-markdown-apps
	   '("Discord"
	     "Element"
	     "Fractal"
	     "NeoChat"
	     "Slack"
	     "thunderbird"
	     "Evolution"
	     ))
  :straight t)

(use-package which-key
  :config (which-key-mode 1))

(use-package company
  :config (global-company-mode 0))

(use-package cape
  :straight t
  :init
  (add-to-list `completion-at-point-functions #'cape-keyword)
  (add-to-list `completion-at-point-functions #'cape-file)
  (add-to-list `completion-at-point-functions #'cape-emoji))

(use-package yasnippet-capf
  :straight t
  :after cape
  :config
  (add-to-list 'completion-at-point-functions #'yasnippet-capf))

(use-package nerd-icons-corfu
  :after corfu
  :init (add-to-list 'corfu-margin-formatters #'nerd-icons-corfu-formatter)
  :straight t)

(use-package corfu
  :demand t
  :custom
  (corfu-auto t)
  (corfu-seperator ?-)
  (corfu-auto-prefix 2)
  :init
  (global-corfu-mode)
  (corfu-history-mode 1)
  (corfu-popupinfo-mode 1))



(use-package smartparens
  :config (require 'smartparens-config)
  :init (smartparens-global-mode))


(use-package projectile
  :straight t
  :init
  (projectile-mode +1)
  :bind (:map projectile-mode-map
              ("C-c p" . projectile-command-map)
	      ("C-c C-p" . projectile-command-map)))

(use-package coterm
  :straight t)

(use-package eat
  :straight t)

(use-package wakatime-mode
  :straight t
  :init (global-wakatime-mode 1))


(use-package editorconfig
  :config (editorconfig-mode 1))

(use-package yasnippet-snippets)
(use-package yasnippet
  :hook (prog-mode . yas-minor-mode))

(use-package nix-mode
  :mode "\\.nix\\'")

(use-package clojure-mode
  :mode "\\.clj[sx]\\'")

(use-package cider)
(use-package clj-refactor)

(use-package envrc
  :init (envrc-global-mode))

(defun my/eglot-capf ()
  (setq-local completion-at-point-functions
              (list
	       (cape-capf-super
		#'eglot-completion-at-point
		#'yasnippet-capf
		#'cape-emoji
		#'cape-file))))



(use-package eglot
  :demand
  :hook (nix-mode . eglot-ensure)
  :defines eglot-server-programs
  :config
  (add-to-list 'eglot-server-programs '(nix-mode . ("nil")))
  (add-hook 'eglot-managed-mode-hook #'my/eglot-capf))
;; Completion modes

(use-package orderless
  :custom (completion-styles '(orderless basic)))

(setq completion-ignore-case t)

(use-package vertico
  :init (vertico-mode))

(use-package marginalia
  :init (marginalia-mode))

(use-package yaml-mode)

(use-package clojure-mode)

(defun reload-nix-installed-pkgs ()
  "Reuild the path to show nix installed packages."
  (interactive nil)
  (load-file "/run/current-system/sw/share/emacs/site-lisp/site-start.el"))


(use-package devdocs)

(use-package exec-path-from-shell
  :config
  :init (setq exec-path-from-shell-variables '("PATH" "MANPATH" "SSH_AUTH_SOCK" ))
  (exec-path-from-shell-initialize))

(use-package all-the-icons-nerd-fonts
  :straight t
  :after all-the-icons
  :demand t
  :config
  (all-the-icons-nerd-fonts-prefer))

(customize-set-variable 'nerd-icons-font-family "Victormono Nerd Font")


(use-package all-the-icons-dired)

(use-package nerd-icons-dired
  :after all-the-icons-dired
  :hook (dired-mode . nerd-icons-dired-mode))

(use-package nerd-icons-completion
  :after marginalia
  :config
    (nerd-icons-completion-mode)
    (add-hook 'marginalia-mode-hook #'nerd-icons-completion-marginalia-setup))

(use-package direnv
 :config
 (direnv-mode))




;; MARKDOWN

(use-package markdown-mode
  :mode ("/README\\(?:\\.md\\)?\\'" . gfm-mode)
  :hook (markdown-mode . turn-on-auto-fill)
  :init
  (setq markdown-enable-math t ; syntax highlighting for latex fragments
        markdown-enable-wiki-links t
        markdown-italic-underscore t
        markdown-asymmetric-header t
        markdown-gfm-additional-languages '("sh")
        markdown-make-gfm-checkboxes-buttons t
        markdown-fontify-whole-heading-line t

        ;; HACK Due to jrblevin/markdown-mode#578, invoking `imenu' throws a
        ;;      'wrong-type-argument consp nil' error if you use native-comp.
        markdown-nested-imenu-heading-index (not (ignore-errors (native-comp-available-p))))
  :config
  (sp-local-pair '(markdown-mode gfm-mode) "`" "`"
                 :unless '(:add sp-point-before-word-p sp-point-before-same-p))
  (cons #'markdown-code-block-at-point-p
                                 fill-nobreak-predicate))
(use-package markdown-toc)

;; JS/TS

(use-package js-ts-mode
  :mode "\\.js\\'")

(use-package typescript-ts-mode
  :mode (("\\.ts\\'" . typescript-ts-mode)
         ("\\.tsx\\'" . tsx-ts-mode)))

(cl-defmethod project-root ((project (head eglot-project)))
  (cdr project))


(defun my-project-try-tsconfig-json (dir)
  (when-let* ((found (locate-dominating-file dir "tsconfig.json")))
    (cons 'eglot-project found)))

(add-hook 'project-find-functions
          'my-project-try-tsconfig-json nil nil)

(add-to-list 'eglot-server-programs
             '((typescript-mode) "typescript-language-server" "--stdio"))

(use-package prettier
  :straight t)

(use-package web-mode
  :mode ("\\.html\\'")
  :config (add-to-list 'eglot-server-programs '(web-mode . ("vscode-html-language-server" "--stdio")))
  :straight t)

;; php
(use-package php-mode
  :mode "\\.php\\'")

;; Security

(auth-source-pass-enable)
(use-package epa
  :config
  (setq epa-file-cache-passphrase-for-symmetric-encryption t)
  (epa-file-enable))

(use-package password-store
  :autoload password-store-get
  :straight t)

;; Email
;; SPELLing

(use-package languagetool
  :commands (languagetool-check
             languagetool-clear-suggestions
             languagetool-correct-at-point
             languagetool-correct-buffer
             languagetool-set-language
             languagetool-server-mode
             languagetool-server-start
             languagetool-server-stop)
  :bind (
	      ("C-c $" . languagetool-correct-at-point))
  :config
  (setq languagetool-java-arguments '("-Dfile.encoding=UTF-8")
        languagetool-console-command "env languagetool-commandline"
        languagetool-server-command "env languagetool-http-server."
	languagetool-server-check-delay 0.5))

(defun  yd/load-language-package (lang)
  (load (s-concat (locate-user-emacs-file "") "/langs/" lang )))
(defun  yd/load-module-package (module)
  (load (s-concat (locate-user-emacs-file "") "/modules/" module )))


(use-package rg
  :straight 't)

;;load modules
(defvar ydl-modules
     '("llm-ai" "email" "org"))

(defvar ydl-langs
  '("php"))

(defvar emacs-config-path "/etc/nixos/home/emacs/")
(defun edit-my-emacs-confg ()
    "opens my emacs config path"
    (interactive)
    (let ((default-directory emacs-config-path))
	 (call-interactively 'find-file )))



(seq-do 'yd/load-module-package
	ydl-modules)

(seq-do 'yd/load-language-package
	ydl-langs)

(use-package ligature
  :straight t
  :config
  (ligature-set-ligatures 't '("www"))
  ;; Enable all Iosevka ligatures in programming modes
  (ligature-set-ligatures
   'prog-mode
   '( "</" "</>" "/>" "~-" "-~" "~@"
      "<~" "<~>" "<~~" "~>" "~~" "~~>"
      ">=" "<=" "<!--" "##" "###" "####" "|-" "-|" "|->" "<-|" ">-|" "|-<"
      "|=" "|=>" ">-" "<-" "<--" "-->" "->" "-<"
      ">->" ">>-" "<<-" "<->" "->>" "-<<" "<-<"
      "==>" "=>" "=/=" "!==" "!=" "<=="
      ">>=" "=>>" ">=>" "<=>" "<=<" "=<=" "=>=" "<<=" "=<<"
      ".-" ".=" "=:=" "=!=" "==" "===" "::" ":=" ":>" ":<" ">:" "<:" ";;"
      "=~" "!~" "::<" "<|" "<|>" "|>" "<>"
      "<$" "<$>" "$>" "<+" "<+>" "+>"
      "?=" "/=" "/==" "/\\" "\\/" "__" "&&" "++" "+++"))
  ;; Enables ligature checks globally in all buffers. You can also do it
  ;; per mode with `ligature-mode'.
  (global-ligature-mode 1))

(use-package default) ;; for some reason needs this for nix


(unless
    (server-running-p)
  (server-start))


