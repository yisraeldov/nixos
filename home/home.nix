{ config, pkgs, ... }:
with builtins;
with import <nixpkgs> { };

let
  unstable = import <nixos-unstable> { };
  hostname = (if lib.pathExists /etc/hostname then
    lib.fileContents /etc/hostname
  else
    "no-hostname");
in {
  # nixpkgs.config.allowlistedLicenses = with lib.licenses;
  #   [ unfreeRedistributable ];
  nixpkgs.config.allowUnfreePredicate = pkg:
    builtins.elem (lib.getName pkg) [
      "slack"
      "steam"
      "steam-original"
      "font-bh-lucidatypewriter"
      "nomachine-client"
      "geekbench"
      "nvidia-x11"
      "unigine-valley"
    ];

  nixpkgs.config.allowUnfree = true;
  imports = (import ./modules-list.nix)
    ++ (if stdenv.isDarwin then [ ./practitest.nix ] else [ ])
    ++ (let hostConfigPath = lib.debug.traceVal (./. + "/${hostname}.nix");
    in (if lib.pathExists hostConfigPath then [ hostConfigPath ] else [ ]));
  linuxDesktop.enable = stdenv.isLinux;
  fonts.fontconfig.enable = true;

  home.packages = with pkgs; [
    # arciving
    unzip
    libunarr
    unrar-wrapper
    dtrx
    unar

    htop
    unstable.btop

    fd
    bat
    ripgrep
    bash-completion
    nix-zsh-completions
    lsd

    wakatime

    #git tools
    git-annex
    git-crypt
    git-ignore
    glab
    gh

    pandoc
    rclone
    culmus # hebrew fonts
    (nerdfonts.override {
      fonts = [
        "FiraCode"
        "VictorMono"
        "JetBrainsMono"
      ];
    })

    unstable.ghostty

    exercism

    #communication
    #schildichat-desktop
    #unstable.nheko
    element-desktop
    #unstable.cinny-desktop
    #unstable.chatty

    #graphics
    inkscape
    gimp
    imagemagick

    #Video
    ffmpeg-full

    unstable.yt-dlp
    unstable.tartube-yt-dlp

    #spelling
    aspell
    aspellDicts.yi
    aspellDicts.he
    aspellDicts.en
    aspellDicts.en-computers
    aspellDicts.en-science

    hunspellDicts.he_IL
    hunspellDicts.en_US
    languagetool
    libreoffice
    unstable.gopass
    unstable.gopass-jsonapi
    browserpass
    #emacsPackages.emacsql
    libvterm
    libnotify
    oauth2ms
    cyrus-sasl-xoauth2
  ];

  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;
  programs.command-not-found.enable = true;

  programs.direnv.enable = true;
  programs.direnv.enableZshIntegration = true;
  programs.direnv.nix-direnv.enable = true;

  programs.chromium = {
    extensions = [
      { id = "eimadpbcbfnmbkopoojfekhnkhdbieeh"; } # dark reader
      { id = "naepdomgkenhinolocfifgehidddafch"; } # browser pass
      { id = "fkeaekngjflipcockcnpobkpbbfbhmdn"; } # copy as markdown
      { id = "kkhfnlkhiapbiehimabddjbimfaijdhk"; } # gopass bridge 
    ];
  };

  services.syncthing = { enable = true; };

  services.activitywatch = {
    enable = false;
  };
  programs.mpv = {
    enable = true;
    config = {
      speed = 1.8;
      save-position-on-quit = true;
      window-maximized = true;
      x11-bypass-compositor = true;
    };
    scripts = with pkgs.mpvScripts; [
      mpris
      webtorrent-mpv-hook
      sponsorblock
      visualizer
    ];
  };

  home.sessionVariables = rec {
    EDITOR = "emacsclient -c";
    SHELL = "${pkgs.zsh}/bin/zsh";
#    QT_QPA_PLATFORMTHEME = "gtk2";
  };

  home.shellAliases = {
    mic-check = " ffmpeg -f pulse -i default  -f matroska  - | ffplay - ";
  };

  # email
  accounts.email.accounts = {
    lebowtech = {
      primary = lib.mkDefault true;
      realName = "Yisrael Dov Lebow";
      address = "lebow@lebowtech.com";
      aliases = [ "lebow@lebowtech.co.il" ];
      gpg.signByDefault = true;
      gpg.key = "defbcfd001b445c1c3751ca06ae703b3e171c686";
      imap.host = "imap.dreamhost.com";
      imap.port = 993;
      smtp.host = "smtp.dreamhost.com";
      smtp.tls.useStartTls = true;
      userName = "lebow@lebowtech.com";
      passwordCommand = "pass lebow@lebowtech.com@mail.dreamhost.com";
      mbsync.enable = true;
      mbsync.create = "both";
      mbsync.expunge = "none";
      mbsync.flatten = ".";
      mbsync.extraConfig.account = {
        PipelineDepth = 10;
        Timeout = 50;
      };
      mbsync.extraConfig.local  ={
        MaxSize = "100K";
      };

      msmtp.enable = true;
      imapnotify = {
        enable = true;
        boxes = ["Inbox"];
        onNotify = "${pkgs.isync}/bin/mbsync lebowtech:INBOX";
        onNotifyPost = "${pkgs.libnotify}/bin/notify-send 'Imap Notify Lebowtech'";
      };

      #mbsync.patterns = [ "Inbox" "Sent" "Archive" "Drafts" "Deleted" ];
      #mbsync.extraConfig.channel.MaxMessages = 100;
      neomutt = {
        enable = true;
        mailboxType = "imap";
        mailboxName = "lebowtech";
      };
      folders = {
        trash = "INBOX.Trash";
        sent = "INBOX.Sent";
      };

    };
    bhbsdventures = {
      realName = "Yisrael Dov Lebow";
      address = "yisraeldov@bhbsdventures.com";
      userName = "yisraeldov@bhbsdventures.com";
      primary = false;
      passwordCommand = "pass hostinger.titan.email";
      imap.host = "imap.titan.email";
      smtp.host = "smtp.titan.email";
      mbsync.enable = true;
      mbsync.create = "both";
      mbsync.expunge = "none";
      mbsync.flatten = ".";
      msmtp.enable = true;
    };
    gmail = {
      primary = false;
      realName = "Yisrael Dov Lebow";
      address = "yisraeldov@gmail.com";
      gpg.signByDefault = true;
      gpg.key = "defbcfd001b445c1c3751ca06ae703b3e171c686";
      flavor = "gmail.com";
      imap.host = "imap.gmail.com";
      smtp.host = "smtp.gmail.com";
      userName = "yisraeldov@gmail.com";
      passwordCommand = "pass imap.gmail.com";
      mbsync.enable = true;
      mbsync.patterns = ["*"];
      mbsync.create = "maildir";
      mbsync.expunge = "maildir";
      mbsync.extraConfig.account = {
        PipelineDepth = 10;
        Timeout = 60;
        AuthMechs= "PLAIN";
      };

      mbsync.extraConfig.local ={
        MaxSize = "100K";
      };
      msmtp.enable = true;
      imapnotify = {
        enable = true;
        boxes = ["Inbox"];
        onNotify = "${pkgs.isync}/bin/mbsync gmail:INBOX";
        onNotifyPost = "${pkgs.libnotify}/bin/notify-send 'Imap Notify gmail'";
      };

      folders = {
        drafts = "[Gmail]/Drafts";
        sent = "[Gmail]/Sent Mail";
        trash = "[Gmail]/Trash";
      };
      neomutt = {
        enable = true;
        mailboxType = "imap";
        mailboxName = "gmail";
      };
    };

  };



  services.mbsync.enable = false;
  programs.mbsync.package = isync.override { withCyrusSaslXoauth2 = true; } ;
  services.imapnotify.enable = true;

  programs.mbsync.enable = true;
  programs.mu.enable = true;
  programs.msmtp.enable = true;
  programs.msmtp.extraConfig = ''
                             defaults
                             logfile        ~/.msmtp.log;

                             '';
  programs.password-store = {
    enable = true;
    package = pkgs.pass.withExtensions (exts: [ exts.pass-otp ]);
  };
  programs.browserpass.enable = true;

  programs.bash = {
    enable = true;
    initExtra = ''
       export NIX_PATH=$NIX_PATH:$HOME/.nix-defexpr/channels
        Use bash-completion, if available
      . ${pkgs.bash-completion}/etc/profile.d/bash_completion.sh
    '';
  };

  programs.lsd = {
    enable = true;
    enableAliases = true;
    settings.date = "relative";
  };

  programs.git = {
    enable = true;
    package = lib.mkDefault pkgs.gitFull;
    signing.signByDefault = true;
    signing.key = "lebow@lebowtech.com";
    userEmail = "lebow@lebowtech.com";
    userName = "Yisrael Dov Lebow";
    ignores = [ "*~" "\\#*\\#" ".\\#*" ];
    delta.enable = true; # fancy diff
    delta.options.side-by-side = false;
    extraConfig = {
      init.defualtBranch = "master";
      push.recurseSubmodules = "check";
      diff.submodule = "log";
      fetch.parallel = 0;
      status.submoduleSummary = 5;
      safe.directory = [ "/etc/nixos" ];
    };
  };

  programs.emacs.enable = true;
  programs.gpg.enable = true;
  langs = [ "fennel" ];

  programs.zsh = {
    enable = true;
    enableCompletion = true;
    defaultKeymap = "emacs";
    envExtra = ''
      [ -f /etc/profile.d/nix.sh ] && . /etc/profile.d/nix.sh
      #export NIX_PATH=$NIX_PATH:$HOME/.nix-defexpr/channels
      #this is for nix on guest os with home manager
      export NIX_PATH=$HOME/.nix-defexpr/channels:/nix/var/nix/profiles/per-user/root/channels:$NIX_PATH
      export SSH_AUTH_SOCK=$(${pkgs.gnupg}/bin/gpgconf --list-dirs agent-ssh-socket)
    '';
  };

  xdg.enable = true;
  xdg.configFile ={
    oauth2ms ={
      enable = true;
      source = ./xdg-config/oauth2ms ;
      target = "oauth2ms";
    };
  };
  programs.starship.enable = true;
  programs.starship.enableZshIntegration = true;
  programs.starship.settings = {
    character = {
      success_symbol = ">";
      error_symbol = "[!](bold red)";
    };
  };

  programs.ssh = {
    enable = true;
  };
  # Home Manager needs a bit of information about you and the
  # paths it should manage.
  home.username = "yisraeldov";
  home.homeDirectory = lib.mkIf (stdenv.isLinux) "/home/yisraeldov";
  # This value determines the Home Manager release that your
  # configuration is compatible with. This helps avoid breakage
  # when a new Home Manager release introduces backwards
  # incompatible changes.
  #
  # You can update Home Manager without changing this value. See
  # the Home Manager release notes for a list of state version
  # changes in each release.
  home.stateVersion = "21.11";

}
