{ config, pkgs, lib, ... }:
with lib;
let unstable = import <nixos-unstable> { };
in {
  home.packages = with pkgs; [
    gnomeExtensions.just-perfection
    gnomeExtensions.pop-shell
    gnomeExtensions.appindicator
  ];
  dconf.settings = {
    "org/gnome/desktop/interface" = { color-scheme = "prefer-dark"; };

    "org/gnome/shell" = {
      disable-user-extensions = false;
      enabled-extensions = [
        "appindicatorsupport@rgcjonas.gmail.com"
        "just-perfection-desktop@just-perfection"
        "pop-shell@system76.com"
      ];
    };
    # move clock to the right side
    "org/gnome/shell/extensions/just-perfection" = { clock-menu-position = 1; };
  };

}
