{ stdenv, undmg, fetchurl, lib, ... }:
with builtins;

stdenv.mkDerivation {
  meta = {
    description = "Desktop client for Slack";
    homepage = "https://steam.com";
    license = lib.licenses.unfree;
    maintainers = [ "lebow@lebowtech.com" ];
    platforms = [ "x86_64-darwin" ];
  };

  name = "commnad-not-found-db";
  src = fetchurl {
    url = "https://cdn.akamai.steamstatic.com/client/installer/steam.dmg";
    sha256 = "1sd0fr9s75zkbl7wfl3w8rsys5k2dfgfx6ib26ca7pgmm4sp2y6s";
  };
  nativeBuildInputs = [ undmg ];
  sourceRoot = "Steam.app";
  installPhase = ''
    mkdir -p $out/Applications/Steam.app
    cp -R . $out/Applications/Steam.app
  '';
}
