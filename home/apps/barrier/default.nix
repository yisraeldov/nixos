{ stdenv, undmg, fetchurl, lib, ... }:
with builtins;

stdenv.mkDerivation {
  meta =  {
    description = "Desktop client for Chomium";
    license = lib.licenses.gpl2;
    maintainers = [ "lebow@lebowtech.com" ];
    platforms = [ "x86_64-darwin" ];
  };

  name = "commnad-not-found-db";
  src = fetchurl {
    url = "https://github.com/debauchee/barrier/releases/download/v2.3.3/Barrier-2.3.3-release.dmg";
    sha256 = "03d437lbsl49r20fhs3jh1p2p5qgmw3z37bvlykynlv4g4nh0spw";
  };
  nativeBuildInputs = [ undmg ];
  sourceRoot = "Barrier.app";
  installPhase = ''
    mkdir -p $out/Applications/Barrier.app
    cp -R . $out/Applications/Barrier.app
  '';
}
