{ stdenv, undmg, fetchurl, lib, ... }:
with builtins;

stdenv.mkDerivation {
  meta =  {
    description = "Desktop client for Chomium";
    license = lib.licenses.bsd3;
    maintainers = [ "lebow@lebowtech.com" ];
    platforms = [ "x86_64-darwin" ];
  };

  name = "commnad-not-found-db";
  src = fetchurl {
    url = "https://github.com/kramred/ungoogled-chromium-macos/releases/download/88.0.4324.150-1.1_x86-64/ungoogled-chromium_88.0.4324.150-1.1_x86-64-macos.dmg";
    sha256 = "523e18e1b0f523f2bcb655808933df2411ec1ef779eb392f77e93d57725cbe4d";
  };
  nativeBuildInputs = [ undmg ];
  sourceRoot = "Chromium.app";
  installPhase = ''
    mkdir -p $out/Applications/Chromium.app
    cp -R . $out/Applications/Chromium.app
  '';
}
