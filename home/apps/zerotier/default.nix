{ stdenv, fetchzip, ... }:
with builtins;
stdenv.mkDerivation {
  name = "commnad-not-found-db";
  src = fetchzip {
    url = "https://nixos.org/channels/nixos-unstable/nixexprs.tar.xz";
    sha256 = "0lpnzjpmnidyzb2aaxnk80jjvzwcjh3frybd5br5kgs981dp9sfa";
  };

  installPhase = ''
                 ls -l
    mkdir $out
    cp -v programs.sqlite $out/
  '';
}
