{ config, pkgs, lib, ... }:
with lib;
let unstable = import <nixos-unstable> { };
in {
  options = {
    home.games.enable = mkEnableOption "Enable games";
  };

  config = mkIf config.home.games.enable {
  home.packages = with pkgs;
    [
      xonotic
      (retroarch.override { cores = with libretro; [
                              citra
                              fbneo
                              snes9x
                              beetle-pce
                              dolphin
                              pcsx2
      ]; })
      eduke32
      libretro-core-info
      gltron
    ] ++ (if stdenv.isLinux then
    # Linix only games
    [
      #tools
      lutris
      protonup-qt
      
      wineWowPackages.stable
      heroic-unwrapped
      
      gamescope

      xorg.xwininfo
      unixtools.xxd
      mangohud

      unstable.openttd
      unstable.hedgewars
      superTuxKart
      # torcs

      #controlorrer support
      sixpair
      jstest-gtk
      antimicrox

      #game engines
#      unstable.solarus
#      unstable.solarus-quest-editor
    ] else
      [ ]);
  };
}
