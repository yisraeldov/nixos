# nix-channel --add  https://nixos.org/channels/nixos-unstable nixos-unstable
# nix-channel --add \
#           https://github.com/nix-community/home-manager/archive/master.tar.gz\
#           home-manager
# sudo nix-channel --add https://github.com/nix-community/home-manager/archive/release-23.05.tar.gz home-manager
# sudo nix-channel --update
# gpg --pinentry-mode=loopback --import <path to key>
# gpg-connect-agent /bye
# gpg-agent --daemon --pinentry-program `which pinentry-curses`
{ pkgs ? import <nixpkgs> {} }:
  pkgs.mkShell {
    # nativeBuildInputs is usually what you want -- tools you need to run
    nativeBuildInputs = with pkgs.buildPackages;
    		      [ emacs gnupg git git-crypt pinentry_curses];
}
