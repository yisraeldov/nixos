# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

let
  secrets = import ./secrets.nix;
  deviceEnv = import ./env.nix;
  deviceName = deviceEnv.deviceName;
  unstable = import <nixos-unstable> { };
in { config, pkgs, lib, ... }: {
  imports = [ # Include the results of the hardware scan.
    <home-manager/nixos>
    ./${deviceName}/configuration.nix
    ./8bitdo.nix
  ];

  nix.optimise.automatic = true;
  nix.optimise.dates = [ "03:45" ];
  nix.settings.experimental-features =[
    "nix-command"
    "flakes"
  ];

  # Enable networking
  networking.networkmanager.enable = true;

  # Set your time zone.
  time.timeZone = "Asia/Jerusalem";

  # Select internationalisation properties.
  i18n.defaultLocale = lib.mkDefault "en_US.UTF-8";

  #  services.xserver.enable = true;
  #services.xserver.displayManager.lightdm.enable = lib.mkDefault true;
  #services.xserver.displayManager.gdm.wayland = false;

  #programs.xwayland.enable = true;
  #programs.hyprland.enable = true;

  services.xserver.desktopManager.cinnamon.enable = lib.mkDefault true;

  # Configure keymap in X11
  services.xserver.xkb = {
    layout = lib.mkDefault "us,il,yi";
    variant = "";
  };
  services.xserver.xkb.options = "grp:caps_toggle,grp_led:scroll";
  services.xserver.xkb.extraLayouts.yi.symbolsFile = ./yi;
  services.xserver.xkb.extraLayouts.yi.languages =
    lib.mkDefault [ "yid" "heb" ];
  services.xserver.xkb.extraLayouts.yi.description = "yiddish";

  # Enable CUPS to print documents.
  services.printing.enable = true;

  # Enable sound with pipewire.
  # TODO: set the default beep
  # pactl upload-sample "/run/current-system/sw/share/cinnamon/sounds/bell.ogg" bell-window-system

  hardware.pulseaudio.enable = false;
  security.rtkit.enable = true;
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
    # If you want to use JACK applications, uncomment this
    #jack.enable = true;

    # use the example session manager (no others are packaged yet so this is enabled by default,
    # no need to redefine it in your config for now)
    #media-session.enable = true;
    extraConfig.pipewire = {
      "99-disable-bell" = {
        context.properties = { module.x11.bell = false; };
      };
    };
  };

  # Enable touchpad support (enabled default in most desktopManager).
  # services.xserver.libinput.enable = true;

  # Allow unfree packages
  nixpkgs.config.allowUnfree = true;

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    emacs
    git
    git-crypt
    bat
    fd
    ripgrep
    w3m
    xterm
    usbutils
    killall
    htop
    lshw
    hebcal
    unstable.ollama
    netbird-ui
    netbird
  ];

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  programs.gnupg.agent = lib.mkDefault {
    enable = true;
    enableSSHSupport = true;
  };

  programs.zsh.enable = true;
  users.defaultUserShell = pkgs.zsh;

  programs.steam.enable = lib.mkDefault false;
  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  services.openssh.enable = true;
  services.openssh.extraConfig = "AllowAgentForwarding yes";
  services.openssh.settings.X11Forwarding = true;

  services.ollama = {
    enable = true;
    package = unstable.ollama ;
  };
  programs.mosh.enable = true;
  services.zerotierone = {
    enable = true;
    joinNetworks = secrets.zerotierone.networks;
  };

  services.netbird.enable = true;

  #zero conf
  services.avahi = {
    enable = true;
    publish.enable = true;
    publish.domain = true;
    publish.userServices = true;
    publish.workstation = true;
    publish.addresses = true;

    nssmdns4 = true;
    reflector = true;
    extraServiceFiles = {
      ssh = "${pkgs.avahi}/etc/avahi/services/ssh.service";
      sftp-ssh = "${pkgs.avahi}/etc/avahi/services/sftp-ssh.service";
    };
  };

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  networking.firewall.enable = false;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "24.05"; # Did you read the comment?

  home-manager.users.yisraeldov = import ./home/home.nix;
  home-manager.backupFileExtension = "backup";
  fonts.packages = with pkgs; [
    open-fonts
    culmus # hebrew fonts
    (nerdfonts.override { fonts = [ "FiraCode" "VictorMono" ]; })
  ];
  services.netbird.enable = true; # for netbird service & CLI

}
